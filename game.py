from random import randint

#we want the code to guess the birth MONTH and YEAR up to 5 times... I belive i should have a
#conter set at 0, and every loop (if incorrect) adds 1 to the counter. As soon as the counter
#reaches 5, and we still havent guessed, it will print "I have other things to do. Good bye."

#input for getting their name
#need a radint for month
#need a radint for year

name = input("Hi! What is your name? ")

def ask_function():
    counter = 0


    month_range = randint(1,12)
    year_range = randint(1924,2004)

    ask_date = input(f"{name}, were you born in {month_range} / {year_range}? Yes or No? ").lower()



    if ask_date == "yes":
        print("I knew it!")
    elif counter == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
        counter += 1
        ask_function()
        #counter is being reset every time I invoke it and thats why it's constantly looping and not stopping after 5 tries.
        #I wanted to try a function instead of doing a TON of if, elif, and else statements

ask_function()
